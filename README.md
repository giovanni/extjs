# Atende ExtJS Extensions
Copyright (c) Atende Tecnologia da Informação.
Projeto contendo extensões para outros projetos que utilizem a interface ExtJS.

## Setup

Configure o Loader do Extjs para localizar as classes do projeto através do método: 

Todos as classes começam com o nome ATD de atende no código abaixo as classes foram colocadas na pasta app/ux

	Ext.Loader.setPath('ATD', 'app/ux');
	
A partir de agora e possível usar as configurações uses e requires do Extjs 4 para carregar os arquivos e instanciar as classes como Ext.create como usual.

Carregue o estilo CSS utilizado pelas classes, considerando o caminho acima o caminho seria: <link rel="stylesheet" href="/app/ux/resources/css/all.css" />

## Exemplo Funcional

index.html:

	<!DOCTYPE html>
	<html>
		<head>
    		<meta charset="UTF-8">
    		<title>Atende CRM</title>
    		<link rel="stylesheet" href="/library/ext/resources/css/ext-all.css" />
    		<link rel="stylesheet" href="/app/ux/resources/css/all.css" />
    		<link rel="stylesheet" href="/recursos/css/standard.css" />
    		<link rel="stylesheet" href="/recursos/css/icons.css" />
    		<script type="text/javascript" src="/library/ext/ext-debug.js"></script>
    		<script type="text/javascript" src="app.js" ></script>
    	</head>
    	<body>
    	</body>
    </html>
 

app.js:

	/**
	 * Criado por Giovanni Candido <giovanni@atende.info>
	 * Date: 23/07/12
	 * Time: 15:27
	 */
	Ext.Loader.setPath('ATD', 'app/ux');
	 Ext.application({
	     requires: ['Ext.container.Viewport','ATD.util.ExceptionView'],
	     name: 'CRM',
	     appFolder: 'app',
	     controllers: ['MenuCtl','ClientCtl'],
	     launch: function(){
	         Ext.create('ATD.util.ExceptionView');
	         Ext.create('Ext.container.Viewport',{
	             layout: 'border',
	             items: [
	                 {
	                     region: 'center',
	                     xtype: 'panel',
	                     id: 'centerPanel'
	                 },{
	                     region: 'north',
	                     xtype: 'panel',
	                     border: false,
	                     layout: 'hbox',
	                     height: 80,
	                     items: [
	                         {
	                             xtype: 'box',
	                             autoEl: {
	                                 tag: 'span',
	                                 cls: 'logo'
	                             },
	                             width: 100,
	                             height: 80
	                         },{
	                             xtype: 'toolbar',
	                             height: 80,
	                             flex: 1,
	                             border: false,
	                             items: [
	                                 {
	                                     title: 'Clientes',
	                                     xtype: 'buttongroup',
	                                     defaults: {
	                                         iconAlign: 'top'
	                                     },
	                                     columns: 2,
	                                     items: [
	                                         {
	                                             text: 'Cadastrar',
	                                             scale: 'large',
	                                             iconCls: 'icon-add-male-user32',
	                                             action: 'addClient'
	                                         },{
	                                             text: 'Procurar',
	                                             iconCls: 'icon-search-male-user32',
	                                             scale: 'large',
	                                             action: 'searchClient'
	                                         }
	                                     ]
	                                 }
	                             ]
	                         }
	                     ]
	                 }
	             ]
	         })
	     }
	 })


