/**
 * Classe para manipular Execoes vindas do servidor globalmente. Para utilizar coloque-a como dependencia
 * em um ponto central com o arquivo app.js e instancie-o uma unica vez com o Ext.create('ATD.util.ExceptionView')
 * Criado por Giovanni Candido <giovanni@atende.info>
 * Date: 26/08/12
 * Time: 17:32
 */
Ext.define('ATD.util.ExceptionView',{
    uses: ['ATD.Notify'],
    singleton: true,
    constructor: function(){
        Ext.util.Observable.observe(Ext.data.Connection);
        Ext.data.Connection.on('requestexception', function(dataconn, response, options){
            if (response.responseText != null) {

                ATD.Notify.msg(response.status + ' - ' +  response.statusText,
                    'Um erro inesperado ocorreu, por favor contate o suporte informando o que fazia ' +
                        'e o codigo de erro ' + response.status + ' - ' + response.statusText, true);
            }
        });
        Ext.data.Connection.on('requestcomplete', function(dataconn, response, options){
            if(response.responseText != null){
                var resposta = Ext.decode(response.responseText);
                if(resposta.message != null){
                    // Se resposta conter o atributo important como true mantem a mensagem na tela
                    var stay = false;
                    if(resposta.important){
                        stay = true
                    }
                    if(resposta.success){
                        ATD.Notify.msg('Ok', resposta.message, stay);
                    }else{
                        ATD.Notify.msg('Erro', resposta.message, stay, 7000);
                    }
                }

            }
        });

    }
})
