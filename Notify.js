/**
 * Classe para gerar mensagens de notificacao na tela
 * Criado por Giovanni Candido <giovanni@atende.info>
 * Date: 24/08/12
 * Time: 14:43
 */

Ext.define('ATD.Notify', {
    uses: ['Ext.fx.Anim'],
    statics: {
        msgCt: '',
        /**
         * Exibe Mensagem
         * @param title Titulo da mensagem
         * @param format Mensagem ou mensagem formatada com @link{Ext.String.format}
         * @param stay Boolean, se a mensagem deve permanecer fixa ou desaparecer em 1 segundo.
         * Se true permanece se false desaparece sendo que por padrão é false.
         */
        msg: function (title, format, stay, delay) {
            stay = typeof stay !== 'undefined' ? stay : false;
            //Delay padrao de 1000 (um segundo)
            delay = typeof delay !== 'undefined' ? delay : 1000;
            if (!this.msgCt) {
                this.msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
            }
            var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m;
            m = Ext.DomHelper.append(this.msgCt, this.createBox(title, s, stay), true);
            if(stay){
                m.down('.close').on('click', function(){
                    this.parent().ghost("t", {remove: true, easing: 'easeIn'});
                })
            }

            m.hide();
            if(stay){
                m.slideIn('t');
            }else{
                m.slideIn('t').ghost("t", { delay: delay, remove:true});
            }

        },
        createBox:function (t, s, remove) {
            // return ['<div class="msg">',
            //         '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
            //         '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>', t, '</h3>', s, '</div></div></div>',
            //         '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
            //         '</div>'].join('');
            if(remove){
                return '<div class="msg"><div class="close">Fechar</div><h3>' + t + '</h3><p>' + s + '</p></div>';
            }
            return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
        }
    }
});