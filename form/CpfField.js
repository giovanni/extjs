/**
* Copyright 2011 Giovanni Candido da Silva <giovanni@giovannicandido.com>
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/

/**
* This Version works on ExtJS 4.x
* Based on http://www.extjs.com.br/forum/index.php?topic=5125.0
* and 
* The original author is unknown
*/
Ext.define('ATD.form.CpfField', {
   uses: ['ATD.form.InputTextMask'],
   extend: 'Ext.form.field.Text',
   alias: ['widget.cpffield'],

   
   onlyNumbers: false,
   initComponent: function(){
      var me = this;

      Ext.apply(Ext.form.VTypes, {
         cpf: function(b, a) {
            return me.validacpf(b);
         },
         cpfText: "CPF inválido!"
      });
 
	  if(me.onlyNumbers){
	  	me.plugins = [new ATD.form.InputTextMask('99999999999')]
	  }else{
	  	me.plugins = [new ATD.form.InputTextMask('999.999.999-99')]
	  }
      Ext.apply(me, { vtype: 'cpf' });
      me.callParent();
   },
  
   validacpf: function(e) {
      if (e == "")
         return true;
      var b;
      s = e.replace(/\D/g, "");
      if (parseInt(s, 10) == 0) {
         return false;
      }

      var iguais = true;
      for (i = 0; i < s.length - 1; i++){
         if (s.charAt(i) != s.charAt(i + 1)){
            iguais = false;
         }
      }

      if (iguais)
         return false;

      var h = s.substr(0, 9);
      var a = s.substr(9, 2);
      var d = 0;
      for (b = 0; b < 9; b++) {
         d += h.charAt(b) * (10 - b);
      }
      if (d == 0) {
         return false;
      }
      d = 11 - (d % 11);
      if (d > 9) {
         d = 0;
      }
      if (a.charAt(0) != d) {
         return false;
      }
      d *= 2;
      for (b = 0; b < 9; b++) {
         d += h.charAt(b) * (11 - b);
      }
      d = 11 - (d % 11);
      if (d > 9) {
         d = 0;
      }
      if (a.charAt(1) != d) {
         return false;
      }
      return true;
   }
});