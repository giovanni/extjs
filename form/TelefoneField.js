/**
 * Classe para criacao de textfield com mascara para telefone
 * Criado por Giovanni Candido <giovanni@atende.info>
 * Date: 06/09/12
 * Time: 01:46
 */
 Ext.define("ATD.form.TelefoneField",{
     uses: ['ATD.form.InputTextMask'],
     extend: 'Ext.form.field.Text',
     alias: ['widget.telefonefield'],
     config: {
         cincoDigitos: false
     },
     initComponent: function(){
         var me = this;
         if(this.cincoDigitos){
             me.plugins = [new ATD.form.InputTextMask('(99) 99999-9999')]
         }else{
             me.plugins = [new ATD.form.InputTextMask('(99) 9999-9999')]
         }

         me.callParent(arguments)
     }
 })