/**
 * Criado por Giovanni Candido <giovanni@atende.info>
 * Date: 09/09/12
 * Time: 11:32
 */
Ext.define('ATD.form.plugin.HelpText', {
    constructor: function(txt){
        this.txt = txt;
        return this;
    },
    init: function (me) {
        this.field  = me;
        var label = me.fieldLabel;
        var xt = me.getXType().substr(4);
        if (xt == 'field' || xt == 'obox') {

            // Get the fieldLabel element
            console.debug('Inside HelpText plugin');
            if(me.rendered){
                this.createHelp;
            }else{
                me.on('render', this.createHelp, this);
            }

            //me.on('destroy', this.destroy(id))
        }
    },// End of Init Function
    createHelp: function(){

        var id = this.field.getId(), lbl = this.field.getFieldLabel();
        var domNode = this.field.labelEl;
        if (domNode) {
            var tipTxt = '<p>' + this.txt + '</p>';
            var tipWidth = domNode.dom.offsetWidth * 2;
            // Create the dom object that contains the question mark image
            var helpDiv = {
                tag:'td',
                id:id + '-help',
                html:'&nbsp;',
                cls: 'icon-help',
                style: 'cursor:help;width:25px;' +
                    'padding:1px 1px 5px 1px;vertical-align:middle'
            } // End var helpDiv
// Add the help div to the document
            Ext.DomHelper.insertAfter(domNode.parent().dom, helpDiv);
// Attach the help tool tip to the help div
            var tip = Ext.create('Ext.tip.ToolTip', {
                target:id + '-help',
                html:tipTxt,
                minWidth:tipWidth,
                title:lbl,
                autoHide:false,
                closable:true,
                closeAction:'hide'
            });
        }// End of if(domNode)
    }
})