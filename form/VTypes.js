/**
 * Criado por Giovanni Candido <giovanni@atende.info>
 * Date: 24/09/12
 * Time: 00:02
 */
Ext.define('ATD.form.VTypes', {
    singleton:true,
    init:function () {

        // VTypes
        // Add the additional 'advanced' VTypes
        Ext.apply(Ext.form.field.VTypes, {
            daterange:function (val, field) {
                var date = field.parseDate(val);

                if (!date) {
                    return false;
                }
                if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                    var start = field.up('form').down('#' + field.startDateField);
                    start.setMaxValue(date);
                    start.validate();
                    this.dateRangeMax = date;
                }
                else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                    var end = field.up('form').down('#' + field.endDateField);
                    end.setMinValue(date);
                    end.validate();
                    this.dateRangeMin = date;
                }
                /*
                 * Always return true since we're only using this vtype to set the
                 * min/max allowed values (these are tested for after the vtype test)
                 */
                return true;
            },

            daterangeText:'Start date must be less than end date',

            password:function (val, field) {
                if (field.initialPassField) {
                    var pwd = field.up('form').down('#' + field.initialPassField);
                    return (val == pwd.getValue());
                }
                return true;
            },

            passwordText:'Senhas não conferem',
            IPAddress:function (v) {
                return /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(v);
            },
            IPAddressText:'Must be a numeric IP address',
            IPAddressMask:/[\d\.]/i
        });

    }
})