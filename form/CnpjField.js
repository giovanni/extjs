/**
* Copyright 2011 Giovanni Candido da Silva <giovanni@giovannicandido.com>
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/

/**
* This Version works on ExtJS 4.x
* Based on http://www.extjs.com.br/forum/index.php?topic=5124.0
* and 
* The original author is unknown
*/
Ext.define('ATD.form.CnpjField', {
   extend: 'Ext.form.field.Text',
   uses: ['ATD.form.InputTextMask'],
   alias: ['widget.cnpjfield'],
   onlyNumbers: false,
   initComponent: function(){
      var me = this;

      Ext.apply(Ext.form.VTypes, {
         cnpj: function(b, a) {
            return me.verificaCNPJ(b);
         },
         cnpjText: "CNPJ não é válido!"
      });
      if(me.onlyNumbers){
      	me.plugins = [new ATD.form.InputTextMask('999999999999999')]
      }else{
      	me.plugins = [new ATD.form.InputTextMask('99.999.999/9999-99')]

      }
	  Ext.apply(me, { vtype: 'cnpj' });

      me.callParent();
   },
   verificaCNPJ: function(a) {
      var me = this;
      if (a == "") return true;

      a = a.replace(/\D/g, "");
      a = a.replace(/^0+/, "");
      if (parseInt(a, 10) == 0) {
         return false;
      } else {
         g = a.length - 2;
         if (me.testaCNPJ(a, g) == 1) {
            g = a.length - 1;
            if (me.testaCNPJ(a, g) == 1) {
               return true;
            } else {
               return false;
            }
         } else {
            return false;
         }
      }
   },
   testaCNPJ: function(a, d) {
      var b = 0;
      var e = 2;
      var c;
      for (f = d; f > 0; f--) {
         b += parseInt(a.charAt(f - 1),10) * e;
         if (e > 8) {
            e = 2;
         } else {
            e++;
         }
      }
      b %= 11;
      if (b == 0 || b == 1) {
         b = 0;
      } else {
         b = 11 - b;
      }
      if (b != parseInt(a.charAt(d),10)) {
         return (0);
      } else {
         return (1);
      }
   }
});